<?php

/**
 * @file
 * Contains theme_by_author.module.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_entity_base_field_info().
 */
function theme_by_author_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() === 'user') {
    $fields['theme'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Theme'))
      ->setDescription(t("The user's custom theme."))
      ->setRequired(FALSE)
      ->setTranslatable(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => DRUPAL_EXTENSION_NAME_MAX_LENGTH,
      ])
      ->setSetting('allowed_values_function', 'theme_by_author_get_allowed_themes')
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 90,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }
  return $fields;
}

/**
 * Callback (allowed_values_function) for 'theme' field of 'user' entities.
 */
function theme_by_author_get_allowed_themes() {
  /** @var \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler */
  $theme_handler = \Drupal::service('theme_handler');
  $themes = $theme_handler->listInfo();
  $options = [];
  foreach ($themes as $machine_name => $theme_info) {
    /** @noinspection PhpUndefinedFieldInspection */
    $options[$machine_name] = $theme_info->info['name'];
  }
  return $options;
}
